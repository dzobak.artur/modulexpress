const { Schema, model } = require('mongoose');

const triangleSchema = new Schema({
  sideLength: {
    type: Number,
    required: true,
  },
  height: {
    type: Number,
    required: true,
  },
  area: {
    type: Number,
    required: true,
  },
}, {
  timestamps: true,
});

const Triangle = model('Triangle', triangleSchema);

module.exports = Triangle;
