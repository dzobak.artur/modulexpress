const { Schema, model } = require('mongoose');

const expressionResultSchema = new Schema({
  a: {
    type: Number,
    required: true,
  },
  n: {
    type: Number,
    required: true,
  },
  result: {
    type: Number,
    required: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

const ExpressionResult = model('ExpressionResult', expressionResultSchema);

module.exports = ExpressionResult;
