const Triangle = require('../models/grade.model');
const ExpressionResult = require('../models/ExpressionResult')
//const calculateExpression = require('./expressionCalculator');
const createError = require('http-errors');

async function create({ sideLength, height, area }) {
    return Triangle.create({ sideLength, height, area });
}

async function getTriangle(triangleId) {
    return Triangle.findById(triangleId);
}

async function updateTriangle(triangleId, { sideLength, height, area }) {
    return Triangle.findByIdAndUpdate(triangleId, { sideLength, height, area }, { upsert: false, new: true });
}

async function deleteTriangle(triangleId) {
    return Triangle.findByIdAndDelete(triangleId);
}

async function getAllTriangles() {
    return Triangle.find();
}
async function saveExpressionResult(a, n, result) {
    return ExpressionResult.create({ a, n, result });
}
module.exports = {
    create,
    getTriangle,
    updateTriangle,
    deleteTriangle,
    getAllTriangles,
    saveExpressionResult
};
