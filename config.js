require('dotenv').config();

const config = {
    port: process.env.PORT || 3007,
    mongodb_uri: process.env.MONGODB_URI || 'mongodb://127.0.0.1:27017/modul1',
};

module.exports = config;