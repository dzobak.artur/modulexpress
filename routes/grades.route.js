const express = require('express');
const router = express.Router();

const controller = require('../controllers/grades.controller'); 
const middleware = require('../middlewares/grades.middleware');

router.route('/')
    .get(controller.getAllTriangles)
    .post(middleware.validateTriangleInput, controller.createTriangle);

router.route('/calculate')
    .post(controller.calculateAndSaveExpression);

router.route('/:triangleId')
    .get( controller.getTriangle)
    .patch( controller.updateTriangle)
    .delete( controller.deleteTriangle);

module.exports = router;
