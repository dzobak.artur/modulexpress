const createError = require('http-errors');

function validateTriangleInput(req, res, next) {
    const { sideLength, height } = req.body;

    if (sideLength <= 0 || height <= 0) {
        return next(createError.BadRequest('Side length and height must be positive numbers'));
    }

    const area = 0.5 * sideLength * height;
    if (area <= 0) {
        return next(createError.BadRequest('Area must be a positive number'));
    }

    
    next();
}

module.exports = {
    validateTriangleInput,
};
