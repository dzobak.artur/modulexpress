const TriangleService = require('../services/grades.service');
const calculateExpression = require('./expressionCalculator');
const createError = require('http-errors');

async function createTriangle(req, res) {
    try {
        const { sideLength, height } = req.body;
        
      
        const area = 0.5 * sideLength * height;

        
        const newTriangle = await TriangleService.create({ sideLength, height, area });

        res.status(200).json({
            status: 200,
            data: newTriangle,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
}

async function getTriangle(req, res) {
    try {
        const { triangleId } = req.params;
        const triangle = await TriangleService.getTriangle(triangleId);

        if (!triangle) {
            return res.status(404).json({
                status: 404,
                message: 'Triangle not found',
            });
        }

        res.status(200).json({
            status: 200,
            data: triangle,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
}

async function updateTriangle(req, res) {
    try {
        const { triangleId } = req.params;
        const { sideLength, height } = req.body;
        
        
        const area = 0.5 * sideLength * height;

        const updatedTriangle = await TriangleService.updateTriangle(triangleId, { sideLength, height, area });

        res.status(200).json({
            status: 200,
            data: updatedTriangle,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
}

async function deleteTriangle(req, res) {
    try {
        const { triangleId } = req.params;
        await TriangleService.deleteTriangle(triangleId);

        res.status(200).json({
            status: 200,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
}

async function getAllTriangles(req, res) {
    try {
        const triangles = await TriangleService.getAllTriangles();

        res.status(200).json({
            status: 200,
            data: triangles,
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err,
        });
    }
}
async function calculateAndSaveExpression(req, res) {
    try {
        const { a, n } = req.body;
        
        const result = calculateExpression(a, n);

        await TriangleService.saveExpressionResult(a, n, result);

        res.status(200).json({
            status: 200,
            result: result
        });
    } catch (err) {
        console.error(err);
        res.status(500).json({
            status: 500,
            error: err.message
        });
    }
}



module.exports = {
    createTriangle,
    getTriangle,
    updateTriangle,
    deleteTriangle,
    getAllTriangles,
    calculateAndSaveExpression
};
