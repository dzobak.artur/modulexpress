function calculateExpression(a, n) {
    let result = 0;

    for (let i = 1; i <= n; i++) {
        let numerator = 1;
        let denominator = a ** (i + 1);
        let denominatorMultiplier = 1;

        for (let j = 1; j <= i; j++) {
            numerator *= j;
            denominatorMultiplier *= a + j - 1;
        }

        result += numerator / (denominator * denominatorMultiplier);
    }

    return result;
}

module.exports = calculateExpression;
